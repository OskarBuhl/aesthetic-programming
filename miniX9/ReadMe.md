### Algorithmic influence
The first idea for our final project came from a discussion about surveillance capitalism and the sorting algorithms of companies like Google and Meta, where we talked about Instagram and Youtube in particular (There are many other examples). The algorithms, in short, sort the content, so that it matches the consumer's earlier watched content. It's simple, but can have an tragic outcome. There are cases of people led to believe in radical opinions as a result of echo chambers and epistemic bubbles (Nguyen, 2018). The program starts with a simple and almost neutral question. The answer of this questions determines the end result already, but the viewer receives many questions as if they all have something to do with the outcome. The program is simple, but should give the viewer the illusion of being brought further and further to an extreme opinion. And the program ends with a result, as for example "YOU ARE AN INCEL", or maybe something less provocative. We wanted to express how important it is to be critical of what you are presented. The consumer might not be aware of the influence of the tailored stream of information from fx. youtube. Therefore it is crucial to be aware.

<img src="algo.png" alt="algo" width="50%"/>

### Population of light
The inspiration of second idea came from the image of cities seen from space at night. To me it is a beautiful sight, a sight of life. It's like every little particle of light is a symbol of a particle of life. We wanted to explore this with generative art, so we set up the following rules:
* **Spawn 1 main particle at random location**
* **Spawn 1 particle in close proximity to the main particle**
* **For every 20 new particle in the a colony spawns a new main particle**
* **If a main particle is in X proximity of another main particle, and if both colonies have X particles they connect by a line of partciles**

<img src="light.png" alt="light" width="50%"/>

### SkyBomber
This is my flowchart of my game "skyBomber". It's a simple game of objects, that you have to avoid, falling down form above. 
<img src="skyBomber.png" alt="skyBomber" width="50%"/>

### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
I believe a large deal of the challenge of communicating the algorithmic procedures is simply choice of words. One misunderstood word can make the conversation split into many disoriented paths. There is also the matter of how well you know your audience. I've found that it can be more difficult to explain the procedure of an algorithm if the person I'm explaining it not very known to me, rather than a person I know well, who understand or knows the meaning and intentions i put into my language and certain words. Another key thing that I've discovered is that language is not enough to create a rich image of how the program will work. It is almost like plain language is the worst method of communicating algorithmic procedure. I've found it very sufficient to draw and mimic the process of the program.

### What are the technical challenges facing the two ideas and how are you going to address these?
With the first idea, we originally wished to make every question and answer relevant for the outcome, but we quickly discovered that the logistics of the program would be way beyond our competence. By making the first question the only one to have impact, we simplified the program without loosing the concept of the program. I don't believe the idea we ended with will be a huge technical challenge to programme, but it will be difficult to place the questions wright in the buttons you can press.

The second idea will we very challenging to create. The rules are very complex because there are many specific conditional statements integrated in them. I suppose that creating the main particles and creating the particles in close proximity will be the easiest, but that still seem like a difficult task. The next steps will be to code that there will spawn a new main particle, which I think will not be very difficult. The last step of creating the connecting lines. I have no idea how to begin with this challenge.

### In which ways are the individual and the group flowcharts you produced useful?
I believe the group flowcharts help us a lot to understand each other, as mentioned in the first question paragraph, and to sure that we were all on the same page. I believe this will be very important when we are going to create our final project. It is difficult, but when it is done, everyone will feel included in the process.
The process of creating the flowchart individually was way less confusing. I still had to argue with myself, but it was way less complicated. In general I believe flowcharts are a great way to visualize your ambitions and intentions for a program. When you break every task down to a few words, the whole picture of the program becomes very clear and less confusing than in your head.

Refrences:
Nguyen, C. Thi. "Escape the echo chamber." Aeon Magazine. https://aeon. co/essays/why-its-as-hard-to-escape-an-echo-chamber-as-it-is-to-flee-a-cult (2018).
